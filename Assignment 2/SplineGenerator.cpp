/*
Author : Samuel Beland-Leblanc - 27185642
Purpose : Handles the generation of the spline(Catmull-Rom) from the control points
*/

#include "SplineGenerator.h"
#include <iostream>

using namespace std;

SplineGenerator::SplineGenerator(float tension, glm::vec3 splineColor) : _spline_color(splineColor)
{
	float s = tension;

	//generate the basis matrix from the tension
	glm::mat4 basis(-s, 2 - s, s - 2, s,
		2 * s, s - 3, 3 - 2 * s, -s,
		-s, 0.0f, s, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f);
	_basis_matrix = new glm::mat4(glm::transpose(basis)); //Transpose since OGL is column major

	//generate the first derivative basis matrix from the tension
	glm::mat4 basisDerivative(0.0f, 0.0f, 0.0f, 0.0f,
		-3 * s, 6 - 3 * s, 3 * s - 6, 3 * s,
		4 * s, 2 * s - 6, 6 - 4 * s, -2 * s,
		s*-1, 0.0f, s, 0.0f);
	_basis_matrix_derivative = new glm::mat4(glm::transpose(basisDerivative)); //Transpose since OGL is column major

	_points = new std::vector<glm::vec3>();
	_colors = new std::vector<glm::vec3>();
}


SplineGenerator::~SplineGenerator()
{
	delete _basis_matrix;
	delete _points;
}

glm::vec3 * SplineGenerator::GetPointerToPointsData()
{
	_has_unread_vertex_data = false; //Clears the flag when data is accessed
	return _points->data();
}

glm::vec3 * SplineGenerator::GetPointerToColorData()
{
	_has_unread_color_data = false; //Clears the flag when data is accessed
	return _colors->data();
}

//Adds a control point for the spline
void SplineGenerator::AddPoint(float x, float y)
{
	//Only add if the spline as not been generated yet
	if (!_spline_generated) {
		glm::vec3 p(x, y, 0.0f);
		_points->push_back(p);
		_colors->push_back(_spline_color);
		//Set to true to transfer control point so that they show on the screen
		_has_unread_vertex_data = true;
		_has_unread_color_data = true;
	}

}

//F'(u) -> get the tangent at a specific point on the spline
glm::vec3 SplineGenerator::FPofU(float u, glm::mat3x4 * controlMatrix)
{
	glm::vec4 uv(pow(u, 3), pow(u, 2), u, 1);

	glm::vec3 res = uv * *_basis_matrix_derivative * *controlMatrix;

	return res;
}

//F(u) -> Get a point on the spline
glm::vec3 SplineGenerator::FofU(float u, glm::mat3x4 * controlMatrix)
{
	glm::vec4 uv(pow(u, 3), pow(u, 2), u, 1);

	glm::vec3 res = uv * *_basis_matrix * *controlMatrix;

	return res;
}

//Subdivide using a max line length limit
void SplineGenerator::Subdivide(float u0, float u1, float maxLineLength, vector<glm::vec3>* points, glm::mat3x4 * controlMatrix)
{
	float uMid = (u0 + u1) / 2;
	glm::vec3 x0 = FofU(u0, controlMatrix);
	glm::vec3 x1 = FofU(u1, controlMatrix);

	if (glm::distance(x0, x1) > maxLineLength) {
		Subdivide(u0, uMid, maxLineLength, points, controlMatrix);
		Subdivide(uMid, u1, maxLineLength, points, controlMatrix);
	}
	else {
		if (points->size() == 0) //To avoid duplicate points
			points->push_back(x0);
		points->push_back(x1);
	}
}

//Subdivide using a max curvature limit
void SplineGenerator::SubdivideCurvature(float u0, float u1, float maxAngle, vector<glm::vec3>* points, glm::mat3x4 * controlMatrix)
{
	float uMid = (u0 + u1) / 2;
	//Get the points
	glm::vec3 x0 = FofU(u0, controlMatrix);
	glm::vec3 x1 = FofU(u1, controlMatrix);
	//Get the tangents at these points
	glm::vec3 tanX0 = glm::normalize(FPofU(u0, controlMatrix));
	glm::vec3 tanX1 = glm::normalize(FPofU(u1, controlMatrix));
	//Dummy unit vector to make a cross produc with the tangent
	glm::vec3 z(0.0f, 0.0f, -1.0f);

	//Find the 2 vectors going towards the center of curvature
	glm::vec3 c0 = glm::cross(z, tanX0);
	glm::vec3 c1 = glm::cross(z, tanX1);

	//Get the angle between the vectors going towards the center of curvature. The greater the angle, the greater the curvature
	float angle = acos(glm::dot(c0, c1));

	if (glm::degrees(angle) > maxAngle || (u0 == 0.0f && u1 == 1.0f)) { //To force at least one subdivision
		SubdivideCurvature(u0, uMid, maxAngle, points, controlMatrix);
		SubdivideCurvature(uMid, u1, maxAngle, points, controlMatrix);
	}
	else {
		if (points->size() == 0) //To avoid duplicate points
			points->push_back(x0);
		points->push_back(x1);
	}
}

//Generate the spline using the Max. line length subdivision
void SplineGenerator::GenerateSpline()
{
	if (!_spline_generated) {
		vector<glm::vec3>* newPoints = new vector<glm::vec3>();
		vector<glm::vec3>* newColors = new vector<glm::vec3>();

		for (int i = 1; i < _points->size() - 2; ++i) {
			//For each control points, get the one before and the next 2
			glm::vec3 p1(_points->at(i - 1).x, _points->at(i - 1).y, 0.0f);
			glm::vec3 p2(_points->at(i).x, _points->at(i).y, 0.0f);
			glm::vec3 p3(_points->at(i + 1).x, _points->at(i + 1).y, 0.0f);
			glm::vec3 p4(_points->at(i + 2).x, _points->at(i + 2).y, 0.0f);

			//Build the control matrix
			glm::mat4x3 controlMatrixTemp(p1, p2, p3, p4);
			glm::mat3x4 controlMatrix = glm::transpose(controlMatrixTemp); //because OGL is column major

			//create the spline with max line length of 5
			Subdivide(0, 1, 5.0f, newPoints, &controlMatrix);

		}
		//Generate the color for the generated spline
		for (int i = 0; i < newPoints->size(); ++i) {
			newColors->push_back(_spline_color);
		}
		//delete old data (basically the control points and their color)
		delete _points;
		delete _colors;
		_points = newPoints;
		_colors = newColors;
		_has_unread_vertex_data = true;
		_has_unread_color_data = true;
		_spline_generated = true;
	}
}

//Generate the spline using the Max. angle(curvature) subdivision
void SplineGenerator::GenerateSplineWithCurvature()
{
	if (!_spline_generated) {
		vector<glm::vec3>* newPoints = new vector<glm::vec3>();
		vector<glm::vec3>* newColors = new vector<glm::vec3>();
		for (int i = 1; i < _points->size() - 2; ++i) {
			//For each control points, get the one before and the next 2
			glm::vec3 p1(_points->at(i - 1).x, _points->at(i - 1).y, 0.0f);
			glm::vec3 p2(_points->at(i).x, _points->at(i).y, 0.0f);
			glm::vec3 p3(_points->at(i + 1).x, _points->at(i + 1).y, 0.0f);
			glm::vec3 p4(_points->at(i + 2).x, _points->at(i + 2).y, 0.0f);

			//Build the control matrix
			glm::mat4x3 controlMatrixTemp(p1, p2, p3, p4);
			glm::mat3x4 controlMatrix = glm::transpose(controlMatrixTemp); //because OGL is column major

			//create the spline with max angle of 2 degrees
			SubdivideCurvature(0, 1, 2.0f, newPoints, &controlMatrix);

		}
		//Generate the color for the generated spline
		for (int i = 0; i < newPoints->size(); ++i) {
			newColors->push_back(_spline_color);
		}
		//delete old data (basically the control points and their color)
		delete _points;
		delete _colors;
		_points = newPoints;
		_colors = newColors;
		_has_unread_vertex_data = true;
		_has_unread_color_data = true;
		_spline_generated = true;
	}
}

void SplineGenerator::Reset() {
	_points->clear();
	_colors->clear();
	_has_unread_vertex_data = true;
	_has_unread_color_data = true;
	_spline_generated = false;
}
