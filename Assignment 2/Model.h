/*
Author : Samuel Beland-Leblanc - 27185642
Purpose : Helper function to generate model matrix for a moving object
*/

#pragma once
#include "glm.hpp"

class Model
{
private:
	glm::mat4 _center_matrix;
	glm::mat4 _orientation_matrix;
	glm::mat4 _position_matrix;
	glm::vec3 _current_orientation;
	glm::vec3 _current_position;
	float _speed;

public:
	Model(int width, int height, float speed);
	~Model();

	glm::vec3 GetPosition() { return _current_position; }
	void Reset();
	void SetOrientation(glm::vec3 orientation);
	void SetPosition(glm::vec3 position);
	void UpdatePositionFromSpeed();
	glm::mat4 GetModelMatrix();

};

