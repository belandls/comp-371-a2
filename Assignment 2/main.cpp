/*
Author : Samuel Beland-Leblanc - 27185642
Purpose : Program driver and handles glfw events
*/

#include "glew.h"
#include "glfw3.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "Shader.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "SplineGenerator.h"
#include "gtx\vector_angle.hpp"
#include "Model.h"

using namespace std;

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 800;

SplineGenerator sg(0.5f, glm::vec3(55.0f/255.0f, 205.0f / 255.0f, 255.0f / 255.0f));

GLenum renderAs = GL_POINTS;		//How to render the spline (POINTS by default)
bool showObject = false;			//Should the object+animation be shown				
glm::mat4 projection;				//Projection matrix used
int fBufferWidth, fBufferHeight;	//Height/width of the frambuffer
Model m(40, 40, 0.5f);				//To handle the model matrix of the moving object
int pointIndex = 0;					//Current point used to calculate the object's orientation


void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	//Adjust the perspective matrix with the new dimensions
	projection = glm::ortho(0.0f, (float)width, 0.0f, (float)height);
	//Adjust the viewport
	glViewport(0, 0, width, height);
	fBufferHeight = height;
	fBufferWidth = width;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	else if (key == GLFW_KEY_P && action == GLFW_PRESS) {
		if (sg.GetSplineGenerated()) {
			renderAs = GL_POINTS;
		}
	}
	else if (key == GLFW_KEY_L && action == GLFW_PRESS) {
		if (sg.GetSplineGenerated()) {
			renderAs = GL_LINE_STRIP;
		}
	}
	else if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS) {
		//Reset the application (delete points, reinitialize model matrix, etc.)
		sg.Reset();
		m.Reset();
		renderAs = GL_POINTS;
		showObject = false;
	}
	else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
		if (!sg.GetSplineGenerated() && sg.GetPoints()->size() >= 4) {
			if (mode & GLFW_MOD_SHIFT) {
				sg.GenerateSplineWithCurvature();
			}
			else {
				sg.GenerateSpline();
			}
		}
	}
	else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
		if (sg.GetSplineGenerated() && !showObject) {
			//Set initial position/orientation of the object at the beginning of the spline
			m.SetPosition(sg.GetPoints()->at(0));
			m.SetOrientation((sg.GetPoints()->at(1) - sg.GetPoints()->at(0)));
			pointIndex = 0;
			showObject = true;
		}
		
	}

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		double x, y;
		glfwGetCursorPos(window, &x, &y);
		sg.AddPoint(x, fBufferHeight - y); //The point will not be added if the spline is generated
	}
}

// The MAIN function, from here we start the application and run the game loop
int main()
{
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Instancing", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions
	glfwGetFramebufferSize(window, &fBufferWidth, &fBufferHeight);
	glViewport(0, 0, fBufferWidth, fBufferHeight);

	//Make the points bigger so that they are easier to see
	glEnable(GL_PROGRAM_POINT_SIZE);
	glPointSize(2);

	Shader s;
	if (s.LoadAndCompileShaders("vertex.shader", "fragment.shader")) {
		glUseProgram(s.GetCurrentProgram());
	}
	else {
		std::cout << "Failed to compile the shaders" << std::endl;
		glfwTerminate();
		return -1;
	}



	//{ VAO[0], VBO[0(vertex)+1(color)] } = > spline
	//{ VAO[1], VBO[2(vertex)+3(color)] } = > object
	GLuint VAO[2], VBO[4];
	glGenVertexArrays(2, VAO);
	glGenBuffers(4, VBO);

	//Bind VAO for spline -----
	glBindVertexArray(VAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);

	//For the vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);

	//for the color data
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	//Unbind VAO for spline -----

	//Define the object's vertices and color to transfer them
	GLfloat triangle[] =
	{
		20.0f, 40.0f, 0.1f,
		10.0f, 0.0f, 0.1f,
		30.0f, 0.0f, 0.1f
	};

	GLfloat triangleColors[] =
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f
	};

	//Bind VAO for object -----
	glBindVertexArray(VAO[1]);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[2]);

	//For vertex adta
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), triangle, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[3]);

	//For color data
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), triangleColors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	//Unbind VAO for objet -----

	//Generate the initial projection matrix. The follow the NDC, (0,0) is at the bottom left and (WIDTH,HEIGHT) is at the top right
	projection = glm::ortho(0.0f, (float)WIDTH, 0.0f, (float)HEIGHT);
	//Get the pos. of the MVP matrix
	GLint mvpPos = glGetUniformLocation(s.GetCurrentProgram(), "mvp"); 

	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		//Only transfer Vertex and Color data if new data is available
		if (sg.GetHasUnreadVertexData()) {
			glBindVertexArray(VAO[0]);
			glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
			glBufferData(GL_ARRAY_BUFFER, sg.GetPointsSizeInByte(), sg.GetPointerToPointsData(), GL_STATIC_DRAW);
			glBindVertexArray(0);
		}
		if (sg.GetHasUnreadColorData()) {
			glBindVertexArray(VAO[0]);
			glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
			glBufferData(GL_ARRAY_BUFFER, sg.GetColorsSizeInByte(), sg.GetPointerToColorData(), GL_STATIC_DRAW);
			glBindVertexArray(0);
		}


		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//For simplicity, always transfer the projection matrix for the spline. The constant transfer is really only needed when the object is rendred also.
		glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(projection));

		//Render spline
		glBindVertexArray(VAO[0]);
		glDrawArrays(renderAs, 0, sg.GetNumberOfPoints());
		glBindVertexArray(0);


		//Render object
		if (showObject) {
			//From a point, orient the object towards the next point until the next point is reached, repeat
			//if we are at the next point
			if (glm::distance(m.GetPosition(), sg.GetPoints()->at(pointIndex + 1)) < 0.5f) {
				//If it's the last point
				if (pointIndex == sg.GetPoints()->size() - 2) {
					//Put the object back at the beginning
					pointIndex = 0;
					m.SetPosition(sg.GetPoints()->at(0));
					m.SetOrientation((sg.GetPoints()->at(1) - sg.GetPoints()->at(0)));
				}
				else {
					//Consider the next 2 points
					pointIndex++;
					//Set the position of the object at the position of the "next" point to avoid orientation imprecision making it so that the objects gets out of the line.
					m.SetPosition(sg.GetPoints()->at(pointIndex));
					//Update orientation with new points pair
					m.SetOrientation((sg.GetPoints()->at(pointIndex + 1) - sg.GetPoints()->at(pointIndex)));
				}
			}
			m.UpdatePositionFromSpeed();

			//Update MVP matrix before drawing
			glm::mat4 mvp = projection * m.GetModelMatrix();
			glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(mvp));

			//draw the object
			glBindVertexArray(VAO[1]);
			glDrawArrays(GL_TRIANGLES, 0, 3);
			glBindVertexArray(0);

		}

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

