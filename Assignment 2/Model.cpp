/*
Author : Samuel Beland-Leblanc - 27185642
Purpose : Helper function to generate model matrix for a moving object
*/

#include "Model.h"
#include "gtc/matrix_transform.hpp"
#include "gtx\transform.hpp"


Model::Model(int width, int height, float speed) : _current_orientation(0.0f,1.0f,0.0f) //Current orientation is considered upward
{
	_speed = speed;
	//Generate center matrix to perform operations(trans./rot.) relative to the object's center 
	_center_matrix = glm::translate(_center_matrix, glm::vec3(width / -2.0f, height / -2.0f, 0.0f)); 
}


Model::~Model()
{
}

void Model::Reset()
{
	_current_orientation = glm::vec3(0.0f, 1.0f, 0.0f); //reset to upward orientation
	_orientation_matrix = glm::mat4();
	_position_matrix = glm::mat4();
}

//Set the orientation of the model
void Model::SetOrientation(glm::vec3 orientation)
{
	//Normalize the orientation for calculation
	glm::vec3 normalizedOri = glm::normalize(orientation);
	//Get the angle from current orientation (to know how much we must rotate the model)
	float angle = acos(glm::dot(normalizedOri, _current_orientation)) * -1.0f;
	//Check that the angle is a valid float. In certain cases, the angle comes back as a value between 0 and the smallest float value, which breaks the orientation matrix.
	if (abs(angle) > std::numeric_limits<float>::min()) {
		//Get the rotation axis from the current and new orientation (cross product)
		glm::vec3 rotationAxis = glm::normalize(glm::cross(normalizedOri, _current_orientation));
		_orientation_matrix = glm::rotate(_orientation_matrix, angle, rotationAxis);
		_current_orientation = normalizedOri;
	}
	
}

//Set the absolute position of the model
void Model::SetPosition(glm::vec3 position)
{
	_current_position = position;
	_position_matrix = glm::translate(glm::mat4(), position);
}

//Update absolute position from current orientation and speed
void Model::UpdatePositionFromSpeed()
{
	SetPosition(_current_position + (_current_orientation * _speed));
}

//Get model matrix from the current position, orientation and center matrix
glm::mat4 Model::GetModelMatrix()
{
	//1. center the object, 2. rotate it, 3. move it to it's position
	return _position_matrix * _orientation_matrix * _center_matrix;
}
