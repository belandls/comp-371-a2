/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Helper class to load the vertex and fragment shader from file, compile them and link them
*/

#include "Shader.h"
#include <fstream>
#include <iostream>
#include <sstream>

Shader::Shader()
{
	_current_program = 0;
}


Shader::~Shader()
{
	glDeleteProgram(_current_program);
}

//Load the shaders' source from file, compile them and links them to a new program
bool Shader::LoadAndCompileShaders(string vertex, string fragment)
{
	//Extract the actual text from the .shader files in order to compile
	string vsContent;
	string fsContent;
	ifstream vsF(vertex.c_str(), ifstream::in);
	ifstream fsF(fragment.c_str(), ifstream::in);

	if (vsF.is_open() && fsF.is_open()) {

		stringstream vsS, fsS;
		vsS << vsF.rdbuf();
		fsS << fsF.rdbuf();
		
		vsContent = vsS.str();
		fsContent = fsS.str();
	}
	else {
		return false;
	}
	
	if (vsF.is_open())
		vsF.close();
	if (fsF.is_open())
		fsF.close();
	
	GLuint vertexShader, fragmentShader;
	GLint success;
	GLchar log[512];

	const GLchar* finalVertexContent = (const GLchar*)vsContent.c_str();
	const GLchar* finalFragmentContent = (const GLchar*)fsContent.c_str();

	//Create a compile the vertex shader from the text read
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &finalVertexContent, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << log << std::endl;
		return false;		
	}
	//Create a compile the fragment shader from the text read
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &finalFragmentContent, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << log << std::endl;
		glDeleteShader(fragmentShader);
		return false;
	}

	//Create a new program and link the 2 shaders
	_current_program = glCreateProgram();
	glAttachShader(_current_program, vertexShader);
	glAttachShader(_current_program, fragmentShader);
	glLinkProgram(_current_program);

	glGetProgramiv(_current_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(_current_program, 512, NULL, log);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << log << std::endl;
		return false;
	}

	//Delete the shaders now that they are linked
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	cout << "Shaders compiled and linked successfully" << endl;

	return true;
}
