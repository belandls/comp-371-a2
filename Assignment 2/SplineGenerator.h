/*
Author : Samuel Beland-Leblanc - 27185642
Purpose : Handles the generation of the spline(Catmull-Rom) from the control points
*/

#pragma once

#include <vector>
#include "glm.hpp"

class SplineGenerator
{
private:
	std::vector<glm::vec3>* _points;
	std::vector<glm::vec3>* _colors;
	bool _spline_generated;
	bool _has_unread_vertex_data;
	bool _has_unread_color_data;
	glm::vec3 _spline_color;

	//Catmull Rom basis functions matrix
	glm::mat4* _basis_matrix;
	//First derivative of the Catmull Rom basis functions matrix
	glm::mat4* _basis_matrix_derivative;

	glm::vec3 FofU(float u, glm::mat3x4* controlMatrix);
	glm::vec3 FPofU(float u, glm::mat3x4* controlMatrix);
	void Subdivide(float u0, float u1, float maxLineLength, std::vector<glm::vec3>* points, glm::mat3x4* controlMatrix);
	void SubdivideCurvature(float u0, float u1, float maxAngle, std::vector<glm::vec3>* points, glm::mat3x4* controlMatrix);

public:
	SplineGenerator(float tension, glm::vec3 splineColor);
	~SplineGenerator();

	bool GetSplineGenerated() { return _spline_generated; }
	bool GetHasUnreadVertexData() { return _has_unread_vertex_data; }
	bool GetHasUnreadColorData() { return _has_unread_color_data; }
	int GetPointsSizeInByte() { return _points->size() * sizeof(glm::vec3); }
	int GetColorsSizeInByte() { return _colors->size() * sizeof(glm::vec3); }
	int GetNumberOfPoints() { return _points->size(); }
	std::vector<glm::vec3>* GetPoints() { return _points; }
	glm::vec3* GetPointerToPointsData();
	glm::vec3* GetPointerToColorData();

	void AddPoint(float x, float y);
	void GenerateSpline();
	void GenerateSplineWithCurvature();
	void Reset();

};


